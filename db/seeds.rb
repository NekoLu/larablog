# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Text.create(name: "sidebar_about_me", text: "About me")
Text.create(name: "is_psychologist_request", text: "Request access to psychologst category")

Text.create(name: "shop_top", text: "Just a little shop!")
Text.create(name: "how_to_buy_product", text: "You should pay for it!")
Text.create(name: "how_to_confirm_payment", text: "Just send me a mail.")

Text.create(name: "about_me_page", text: "I am the best!")
Text.create(name: "contacts_page", text: "My number is 8-800-555-35-35")

Text.create(name: "psychologist_prove", text: "Prove yourself!")

Text.create(name: "contacts_sidebar", text: "My contacts will be here soon!")

Text.create(name: "how_i_work__how_can_i_help", text: "Чем я могу помочь")
Text.create(name: "how_i_work__formats_and_price", text: "Форматы работы и цены")
Text.create(name: "how_i_work__projects", text: "Мои проекты")
Text.create(name: "how_i_work__my_education", text: "Образование")
Text.create(name: "how_i_work__reviews", text: "She is very cool! Best psychologist ever! (c) Me")