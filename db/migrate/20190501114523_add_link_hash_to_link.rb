class AddLinkHashToLink < ActiveRecord::Migration[5.2]
  def change
    add_column :links, :link_hash, :string
  end
end
