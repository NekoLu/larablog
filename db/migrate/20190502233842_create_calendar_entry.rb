class CreateCalendarEntry < ActiveRecord::Migration[5.2]
  def change
    create_table :calendar_entries do |t|
      t.integer :day
      t.integer :number
      t.integer :status, defult: 0
      t.integer :price
    end
  end
end
