class RemoveAllowCommentsFromPosts < ActiveRecord::Migration[5.2]
  def change
    remove_column :posts, :allow_comments
  end
end
