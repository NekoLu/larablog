class CreateProductReviews < ActiveRecord::Migration[5.2]
  def change
    create_table :product_reviews do |t|
      t.text :text
      t.string :author
      t.references :product

      t.timestamps
    end
  end
end
