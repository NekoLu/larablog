class AddProductsTexts < ActiveRecord::Migration[5.2]
  def up
    Text.create(name: "shop_top", text: "Just a little shop!")
    Text.create(name: "how_to_buy_product", text: "You should pay for it!")
    Text.create(name: "how_to_confirm_payment", text: "Just send me a mail.")
  end
end
