class SetupCalendar < ActiveRecord::Migration[5.2]
  def up
    for i in 0..11 do
      for j in 0..19 do
        CalendarEntry.create(day: i, number: j)
      end
    end
  end
  def down
    CalendarEntry.all.each(&:destroy)
  end
end
