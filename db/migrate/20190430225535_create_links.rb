class CreateLinks < ActiveRecord::Migration[5.2]
  def change
    create_table :links do |t|
      t.references :product, foreign_key: true
      t.datetime :expiration

      t.timestamps
    end
  end
end
