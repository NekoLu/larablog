class AddActivatedAndActivationHashToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :activated, :boolean
    add_column :users, :activation_hash, :string
  end
end
