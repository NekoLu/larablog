class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.references :user, foreign_key: true, null: false
      t.references :parent, foreign_key: { to_table: :comments }
      t.references :post, foreign_key: true, null: false
      t.text :text, null: false
    end
  end
end
