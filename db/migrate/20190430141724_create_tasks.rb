class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.integer :type
      t.integer :status
      t.json :payload

      t.timestamps
    end
  end
end
