class AddAllowCommentsToPosts < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :allow_comments, :boolean, default: true
  end
end
