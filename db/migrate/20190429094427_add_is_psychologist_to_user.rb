class AddIsPsychologistToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :is_psychologist, :boolean
  end
end
