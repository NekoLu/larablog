class HowIWorkTexts < ActiveRecord::Migration[5.2]
  def up
    amp = Text.find_by(name: "about_me_page")
    amp.destroy if amp

    Text.create(name: "how_i_work__how_can_i_help", text: "Чем я могу помочь")
    Text.create(name: "how_i_work__formats_and_price", text: "Форматы работы и цены")
    Text.create(name: "how_i_work__projects", text: "Мои проекты")
    Text.create(name: "how_i_work__my_education", text: "Образование")
  end
end
