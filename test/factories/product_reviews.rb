FactoryBot.define do
  factory :product_review do
    text { "MyText" }
    author { "MyString" }
  end
end
