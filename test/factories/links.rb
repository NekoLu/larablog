FactoryBot.define do
  factory :link do
    product { nil }
    expiration { "2019-04-30 22:55:35" }
  end
end
