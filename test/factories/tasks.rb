FactoryBot.define do
  factory :task do
    type { 1 }
    status { 1 }
    payload { "" }
  end
end
