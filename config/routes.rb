Rails.application.routes.draw do

  resources :reviews, only: [:edit, :create, :update, :destroy]
  post 'images/upload'
  get 'pages/how_i_work'
  get 'pages/partnership'
  get 'pages/contacts'
  get 'links/:hash', to: 'links#show'
  resources :products, only: %i[index new edit create update destroy]
  get 'tasks', to: 'tasks#index'
  get 'tasks/:id/decline', to: 'tasks#decline'
  get 'tasks/:id/accept', to: 'tasks#accept'
  resources :texts, only: %i[index create update destroy]
  resources :users do
    get 'activate', to: 'users#activate'
    get 'resend', to: 'users#resend'
  end
  post '/login', to: 'session#login'
  get '/logout', to: 'session#logout'

  get '/search', to: 'posts#search'
  get '/tags/:tag', to: 'posts#tags_search'

  resources :posts, only: %i[index show create update destroy new edit]
  resources :comments
  post '/posts/:id/read', to: 'posts#read'

  get '/psychologist', to: 'posts#psychologist_index'
  get '/book', to: 'posts#book_index'

  root 'posts#index'
end
