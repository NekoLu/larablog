FactoryBot.define do
  factory :post do
    title { Faker::Lorem.sentence(1) }
    short_text  { Faker::Lorem.paragraph(3, true, 4) }
    text { Faker::Lorem.paragraph(30, true, 4) }
  end
end