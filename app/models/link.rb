require 'digest'
require 'securerandom'

class Link < ApplicationRecord
  belongs_to :product
  before_save :set_link_hash

  private

  def set_link_hash
    self.link_hash = Digest::MD5.hexdigest SecureRandom.hex(28)
  end
end
