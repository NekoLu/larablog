class Task < ApplicationRecord
  default_scope { order(id: :desc) }

  enum task_type: [:psychologist_request]
  enum status: [:active, :declined, :complete]

  validates :task_type, presence: true
end
