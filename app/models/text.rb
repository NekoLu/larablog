class Text < ApplicationRecord
  validates :text, presence: true
  validates :name, presence: true
end
