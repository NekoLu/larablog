require 'digest'
require 'securerandom'

class User < ApplicationRecord
  include Rails.application.routes.url_helpers

  has_one_attached :avatar

  has_many :posts, dependent: :delete_all
  has_many :comments, dependent: :delete_all

  validates :name, presence: true, length: {
      in: 1..50
  }, format: {with: /[а-яА-Яa-zA-Z]/}

  validates :surname, presence: true, length: {
      in: 1..50
  }, format: {with: /[а-яА-Яa-zA-Z]/}

  validates :mail, presence: true, format: {
      with: /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
  }, uniqueness: true

  validates :password, presence: true, length: {
      minimum: 8
  }

  before_save :hash_password, if: proc {|u| u.password_changed?}
  before_save :set_activation, if: proc {|u| !u.activated_changed?}



  def as_json(*)
    super.except('password', 'password_salt', 'updated_at').merge({
                                                                      full_name: full_name,
                                                                      avatar: avatar_url
                                                                  })
  end

  def phash(s)
    Digest::SHA256.hexdigest password_salt + s
  end

  def check_password(s)
    return nil unless s.instance_of? String

    phash(s) == password
  end

  def full_name
    name.capitalize + ' ' + surname.capitalize
  end

  def avatar_url
    if avatar.attached?
      rails_blob_path(avatar, disposition: "attachment", only_path: true)
    else
      "https://www.drupal.org/files/issues/default-avatar.png"
    end
  end

  private

  def hash_password
    self.password_salt = SecureRandom.hex
    self.password = phash(password)
  end

  def set_activation
    self.activation_hash = Digest::SHA256.hexdigest DateTime.now.to_i.to_s + SecureRandom.hex
  end
end
