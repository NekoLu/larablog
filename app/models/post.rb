class Post < ApplicationRecord
  include PgSearch

  has_one_attached :preview

  has_many :comments, dependent: :delete_all

  enum category: [:blog, :psychologist, :book]

  default_scope { order(id: :desc) }

  scope :any_tags, -> (tags){ where('tags && ARRAY[?]::varchar[]', tags) }
  scope :all_tags, -> (tags){ where('tags @> ARRAY[?]::varchar[]', tags) }

  pg_search_scope :search_everywhere, against: [:title, :short_text, :text]

  validates :short_text, presence: true
  validates :text, presence: true
  validates :title, presence: true

  def comments_tree
    flat_tree = []
    comments.where(parent: nil).each do |comment|
      flat_tree.push comment

      childs_to_visit = comment.children.to_a
      while childs_to_visit.present?
        current_node = childs_to_visit.shift
        flat_tree.push current_node
        childs_to_visit = current_node.children + childs_to_visit
      end

    end

    i = nil

    flat_tree.each_with_index do |comm, j|
      if comm.is_stacked
        if i.nil?
          i = j
        end
      elsif !i.nil?
        p "SORT! #{i} #{j-1}"
        p flat_tree[i..j-1]
        flat_tree[i..j-1] = flat_tree[i..j-1].sort_by! {|c| c.id}
        p flat_tree[i..j-1]
        p flat_tree[i..j-1].sort_by! {|c| c.id}
        i = nil
      end
    end

    flat_tree
  end

  def allow_comments
    category == "psychologist"
  end
end
