class Comment < ApplicationRecord
  belongs_to :post
  belongs_to :user

  belongs_to :parent, class_name: 'Comment', foreign_key: 'parent_id', optional: true
  has_many :children, class_name: 'Comment', foreign_key: 'parent_id', dependent: :destroy

  before_save :check_allowed

  validates :text, presence: true

  def real_level
    parent.nil? ? 0 : parent.real_level + 1
  end

  def level
    if parent.nil?
      return 0
    else
      return parent.level > 2 ? 3 : parent.level + 1
    end
  end

  def display_level
    is_stacked ? level+1 : level
  end

  def all_children
    childs_to_visit = children.to_a
    childs_to_return = []
    while childs_to_visit.present?
      current_node = childs_to_visit.shift
      childs_to_return.push current_node
      childs_to_visit = current_node.children + childs_to_visit
    end
    childs_to_return
  end

  def all_children_recursion
    children.flat_map do |child_cat|
      child_cat.all_children_recursion << child_cat
    end
  end

  def is_stacked
    level != real_level
  end

  private

  def check_allowed
    unless post.allow_comments
      errors.add(:forbidden, "comments in this post are not allowed")
    end
  end
end
