class Product < ApplicationRecord
  include Rails.application.routes.url_helpers

  default_scope { order(id: :desc) }

  has_one_attached :image

  has_many :links
  has_many :reviews, foreign_key: "product_id", class_name: "ProductReview"

  validates :name, presence: true
  validates :price, presence: true

  def image_url
    if image.attached?
      rails_blob_path(image, disposition: "attachment", only_path: true)
    else
      "https://www.madsquirrel.uk/assets/placeholders/Placeholder-SQ.jpg"
    end
  end
end
