class ProductReview < ApplicationRecord
  belongs_to :product

  validates :text, presence: true
  validates :author, presence: true
  validates :product, presence: true
end
