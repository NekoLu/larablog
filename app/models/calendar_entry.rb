class CalendarEntry < ApplicationRecord
  enum status: [:disabled, :active]
  enum type: [:always, :once]
end
