$(document).on('turbolinks:load', () => {
    let trigger = document.querySelector('#add_product_trigger')
    if (!trigger) return
    trigger.addEventListener('click', e => {
        e.preventDefault()
        trigger.parentElement.parentElement.querySelector('form').classList.toggle('active')
    })
})

$(document).on('turbolinks:load', () => {
    let products = document.querySelectorAll('.product_wrapper')
    if (!products.length) return
    let buy_modal = document.querySelector('#modal_buy')
    products.forEach(product_wrapper => {
        product_wrapper.querySelector('.buy').addEventListener('click', e => {
            buy_modal.querySelector('.tab[data-tabid="buy"]').classList.add('active')
            buy_modal.querySelector('.tab[data-tabid="confirm"]').classList.remove('active')
            buy_modal.querySelector('.product_name').innerText = product_wrapper.dataset.name
            buy_modal.querySelector('.price').innerText = product_wrapper.dataset.price
        })

        let r_t = product_wrapper.querySelector('.reviews_trigger')
        if (r_t) {
            $(product_wrapper).find("#product_review_text").summernote({height: 150, placeholder: "Текст отзыва"})
            r_t.addEventListener('click', e => {
                $('.note-toolbar-wrapper').removeAttr('style')
                $('.note-toolbar').removeAttr('style')
                console.log("CLICK")
                e.preventDefault()
                product_wrapper.querySelector('.reviews_wrapper').classList.toggle('active')
            })
        } else {
            console.error("NO")
        }
        $()
    })
})
