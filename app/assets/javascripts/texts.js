function initTexts() {
    if (!location.pathname === "/texts") {
        console.log("NO TEXTS! D:")
        return
    }

    let text_toggles = document.querySelectorAll(`#texts .toggle`)
    text_toggles.forEach(trigger => {
        trigger.addEventListener('click', e => {
            e.preventDefault()
            trigger.parentElement.parentElement.parentElement.classList.toggle("active")
            $('.note-toolbar-wrapper').removeAttr('style')
            $('.note-toolbar').removeAttr('style')
        })
    })

    let forms = document.querySelectorAll(`#texts form`)
    forms.forEach(form => {
        let textarea = form.querySelector('textarea')
        let textarea_initial = textarea.value

        let submit = form.querySelector('[type="submit"]')
        window.sbm = submit

        // submit.addEventListener('click', e => {
        //     if (!form.classList.contains('changed'))
        //         e.preventDefault()
        // })
        //
        // $(textarea).on("summernote.keyup", function (we, e) {   // callback as jquery custom event
        //     setTimeout(function () {
        //         console.log(we, e)
        //         if (textarea.value != textarea_initial) {
        //             form.classList.add('changed')
        //             submit.disaled = false
        //         } else {
        //             form.classList.remove('changed')
        //             submit.disaled = true
        //         }
        //     }, 200)
        // })

        form.addEventListener('ajax:success', (e, data, status, xhr) => {
            console.log(e, data, status, xhr)
            location.reload()
        })


        $(textarea).summernote({
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['picture', 'video', 'link', 'hr']]
            ],
            fontSizes: ['8', '9', '10', '11', '12', '14', '16', '18', '20', '24', '36', '48' , '64', '82', '150'],
            height: 250,
            placeholder: "Место для текста",
        })
    })
}