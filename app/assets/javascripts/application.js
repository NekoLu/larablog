// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require jquery
//= require jquery_ujs
//= require popper
//= require bootstrap
// require summernote/summernote-bs4.min
//= require activestorage
//= require_tree .

const capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}

function initSticky() {
    // return
    if ($("#container").height() <= $(".sidebar__inner").height()) return
    if ($(window).width() > 1000) {
        console.log('BIG')
        window.sS = new StickySidebar("#sidebar", {
            containerSelector: '#wrapper',
            innerWrapperSelector: '.sidebar__inner',
            topSpacing: 16,
            bottomSpacing: 16
        })
    } else if (window.sS) {
        console.log("small")
        window.sS.destroy()
        window.sS = null
        delete window.sS
    }
}

function initReply() {

}

function initComments() {
    let form = document.querySelector(`#new_comment_form`)
    let edit_form = document.querySelector(`#edit_comment_form`)
    $('#new_comment_form textarea').summernote({
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'strikethrough', 'underline', 'clear']],
            ['fontsize', ['fontsize', 'fontname', 'style']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['insert', ['picture', 'video', 'link', 'hr']]
        ],
        fontSizes: ['8', '9', '10', '11', '12', '14', '16', '18', '20', '24', '36', '48', '64', '82', '150'],
        placeholder: "Напишите комментарий",
        height: 150
    })
    if (!form || !edit_form) return

    document.querySelector(`#add_comment_trigger`).addEventListener('click', e => {
        e.preventDefault()
        form.classList.toggle('active')
        form.style.paddingLeft = "0px"
        if (!document.querySelector(`#add_comment_wrapper form`)) {
            document.querySelector(`#add_comment_wrapper`).append(form)
            $('.note-toolbar-wrapper').removeAttr('style')
            $('.note-toolbar').removeAttr('style')
            form.classList.add('active')
        }
    })

    let reply_triggers = document.querySelectorAll(`.comment .reply`)
    reply_triggers.forEach(trigger => {
        trigger.addEventListener('click', e => {
            form.style.paddingLeft = "16px"
            e.preventDefault()
            let pid = form.querySelector('#comment_parent_id')
            if (pid.value === trigger.dataset.id) {
                form.classList.toggle('active')
            } else {
                trigger.parentElement.parentElement.parentElement.append(form)
                $('.note-toolbar-wrapper').removeAttr('style')
                $('.note-toolbar').removeAttr('style')
                form.classList.add('active')
                pid.value = trigger.dataset.id
            }
            if (form.classList.contains('active')) {
                $('#new_comment_form .note-editable').trigger('focus')
            }
        })
    })

    let edit_triggers = document.querySelectorAll(`.comment .edit`)
    edit_triggers.forEach(trigger => {
        trigger.addEventListener('click', e => {
            e.preventDefault()
            form.classList.remove('active')
            if (edit_form.dataset.id === trigger.dataset.id) {
                edit_form.classList.toggle('active')
            } else {
                trigger.parentElement.parentElement.parentElement.append(edit_form)
                $("#edit_comment_form textarea").summernote("destroy")
                $("#edit_comment_form textarea").summernote("code", trigger.parentElement.parentElement.querySelector('.content').innerHTML)
                $('.note-toolbar-wrapper').removeAttr('style')
                $('.note-toolbar').removeAttr('style')
                edit_form.classList.add('active')
                edit_form.dataset.id = trigger.dataset.id
                edit_form.action = '/comments/' + trigger.dataset.id
            }
            if (edit_form.classList.contains('active')) {
                $('#edit_comment_form .note-editable').trigger('focus')
            }
        })
    })

    let delete_triggers = document.querySelectorAll(`.comment .delete`)
    delete_triggers.forEach(trigger => {
        trigger.addEventListener('click', e => {
            e.preventDefault()
            if (!confirm("Вы точно хотите удалить этот комментарий?")) return
            $.ajax(`/comments/${trigger.dataset.id}`, {method: "delete"}).done(() => location.reload())
        })
    })

    form.addEventListener('ajax:success', (e, data, status, xhr) => {
        console.log(e, data, status, xhr)
        location.href = location.origin + location.pathname + `#comment${e.detail[0].result.comment.id}`
        location.reload()
    })
    form.addEventListener('ajax:error', (e, xhr, status, error) => {
        form.parentElement.querySelectorAll('.errors .error').forEach(error => error.remove())
        let result = e.detail[0]
        Object.keys(result.errors).forEach(error_key => {
            result.errors[error_key].forEach(error => {
                let error_el = document.createElement('div')
                error_el.classList.add('error')
                error_el.innerText = `${capitalize(error_key)}: ${capitalize(error)}`
                form.querySelector('.errors').appendChild(error_el)
            })
        })
        console.error(e.detail[0])
    })

    edit_form.addEventListener('ajax:success', (e, data, status, xhr) => {
        console.log(e, data, status, xhr)
        location.hash = `#comment${e.detail[0].result.comment.id}`
        location.reload()
    })
    edit_form.addEventListener('ajax:error', (e, xhr, status, error) => {
        form.parentElement.querySelectorAll('.errors .error').forEach(error => error.remove())
        let result = e.detail[0]
        Object.keys(result.errors).forEach(error_key => {
            result.errors[error_key].forEach(error => {
                let error_el = document.createElement('div')
                error_el.classList.add('error')
                error_el.innerText = `${capitalize(error_key)}: ${capitalize(error)}`
                form.querySelector('.errors').appendChild(error_el)
            })
        })
        console.error(e.detail[0])
    })
}

function initUserModal() {
    $("#edit_user_description").summernote({
        height: 150,
        placeholder: "Расскажите о себе"
    })
    let modal = document.querySelector(`#modal_user`)
    document.querySelectorAll(`[data-userid]`).forEach(trigger => {
        trigger.addEventListener('click', e => {
            e.preventDefault()
            $.ajax(`/users/${trigger.dataset.userid}`).then((r) => {
                let user = r.result.user
                modal.querySelector(`.fullname`).innerHTML = user.full_name
                modal.querySelector(`.description`).innerHTML = user.description || "<i>Пользователь ничего о себе не написал</i>"
                modal.querySelector(`.avatar`).src = user.avatar
                modal.parentElement.classList.add('active')
            })
        })
    })
}

function sendFile(file, toSummernote) {
    var data;
    data = new FormData;
    data.append('file', file);
    $.ajax({
      data: data,
      type: 'POST',
      url: '/images/upload',
      cache: false,
      contentType: false,
      processData: false,
      success: function(data) {
        console.log('file uploading...');
        if (typeof data.errors !== 'undefined' && data.errors !== null) {
          console.log('ops! errors...');
          return $.each(data.errors, function(key, messages) {
            return $.each(messages, function(key, message) {
              return alert(message);
            });
          });
        } else {
          console.log('inserting image in to editor...');
          return toSummernote.summernote('insertImage', data.url);
        }
      }
    });
  }

$(document).ready(() => {
    console.log('READY')
    // initSticky()
    // initTabs()
    // initModals()
//    $(document).on('turbolinks:load', () => {
        console.log('TURBO LOAD')
        initComments()
        initModals()
        initTabs()
        initSticky()
        initLeft()
        initUserModal()
        if (location.hash.match(/#comment\d+/)) {
            var yOffset = $(location.hash).offset().top
            $(window).scrollTop(yOffset)
        }
        document.body.onmouseover = document.body.onmouseout = handler
        initTexts()
        document.querySelectorAll('[data-editor="summernote"]').forEach(area => {
            $(area).summernote({
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'strikethrough', 'underline', 'clear']],
                    // ['fontsize', ['fontsize', 'fontname', 'style']],
                    // ['color', ['color']],
                    // ['para', ['ul', 'ol', 'paragraph']],
                    // ['height', ['height']],
                    ['insert', ['picture', 'video', 'link', 'hr']]
                ],
                fontSizes: ['8', '9', '10', '11', '12', '14', '16', '18', '20', '24', '36', '48', '64', '82', '150'],
                height: 250,
                placeholder: area.placeholder,
                followingToolbar: area.dataset.following=="false"? false : true,
                callbacks: {
                    onImageUpload: (files) => {
                        console.log(files, area)
                        Array.from(files).forEach(file => sendFile(file, $(area)))
                    }
                }
                
            })
        })
 //   })
    $(document).on('turbolinks:before-render', () => {
        console.log('TURBO DR')
        if (window.sS) {
            window.sS.destroy()
            window.sS = undefined
            delete window.sS
        }
    })
})

function initTabs() {
    document.querySelectorAll('.modal_wrapper').forEach(modal => {
        modal.querySelectorAll(`.tab_trigger`).forEach((trigger) => {
            trigger.addEventListener('click', (e) => {
                e.preventDefault()
                modal.querySelectorAll(`.tab`).forEach(tab => tab.classList.remove('active'))
                modal.querySelectorAll(`.tab_trigger`).forEach(t_t => t_t.classList.remove('active'))
                modal.querySelector(`.tab[data-tabid="${trigger.dataset.tabid}"]`).classList.add('active')
                trigger.classList.add('active')
            })
        })
    })

    document.querySelectorAll('.with_tabs').forEach(wrapper => {
        wrapper.querySelectorAll(`.tab_trigger`).forEach((trigger) => {
            trigger.addEventListener('click', (e) => {
                e.preventDefault()
                wrapper.querySelectorAll(`.tab`).forEach(tab => tab.classList.remove('active'))
                wrapper.querySelectorAll(`.tab_trigger`).forEach(t_t => t_t.classList.remove('active'))
                wrapper.querySelector(`.tab[data-tabid="${trigger.dataset.tabid}"]`).classList.add('active')
                trigger.classList.add('active')
            })
        })
    })
}

function initModals() {
    document.querySelectorAll(`.modal_wrapper`).forEach((wrapper) => {
        wrapper.querySelector('.close').addEventListener('click', (e) => {
            e.preventDefault()
            wrapper.classList.remove('active')
        })
        document.querySelectorAll(`a[data-modalname="${wrapper.dataset.modalname}"]`).forEach(trigger => {
            trigger.addEventListener('click', (e) => {
                e.preventDefault()
                wrapper.classList.add('active')
            })
        })
    })
    document.querySelectorAll('#modal_login form').forEach(form => {
        form.addEventListener('ajax:success', (e, data, status, xhr) => {
            console.log(e, data, status, xhr)
            location.reload()
        })
        form.addEventListener('ajax:error', (e, xhr, status, error) => {
            form.parentElement.querySelectorAll('.errors .error').forEach(error => error.remove())
            let result = e.detail[0]
            Object.keys(result.errors).forEach(error_key => {
                result.errors[error_key].forEach(error => {
                    let error_el = document.createElement('div')
                    error_el.classList.add('error')
                    error_el.innerText = `${capitalize(error_key)}: ${capitalize(error)}`
                    form.parentElement.querySelector('.errors').appendChild(error_el)
                })
            })
            console.error(e.detail[0])
        })
    })
    document.querySelectorAll('.ajax_form').forEach(form => {
        form.addEventListener('ajax:success', (e, data, status, xhr) => {
            console.log(e, data, status, xhr)
            location.reload()
        })
        form.addEventListener('ajax:error', (e, xhr, status, error) => {
            form.parentElement.querySelectorAll('.errors .error').forEach(error => error.remove())
            let result = e.detail[0]
            Object.keys(result.errors).forEach(error_key => {
                result.errors[error_key].forEach(error => {
                    let error_el = document.createElement('div')
                    error_el.classList.add('error')
                    error_el.innerText = `${capitalize(error_key)}: ${capitalize(error)}`
                    form.parentElement.querySelector('.errors').appendChild(error_el)
                })
            })
            console.error(e.detail[0])
        })
    })
}


$(window).resize(initSticky)

if (location.pathname.match(/\/posts\/\d+/)) {
    let id = location.pathname.match(/\/posts\/(\d+)/)[1]
    Rails.ajax({
        url: `/posts/${id}/read`,
        type: "POST",
        success: function (data) {
            console.log(data)
        }
    })
}

$(document).ready(() => initLeft())

function initLeft() {
    if (!document.querySelector('#file_preview')) return
    document.querySelector('#file_preview').addEventListener("change", (e) => {
        console.log(e)
        let loadImage = e.target
        if (loadImage.files && loadImage.files[0]) {
            var reader = new FileReader()

            reader.onload = function (e) {
                $('#img_load').attr('src', e.target.result)
            }

            reader.readAsDataURL(loadImage.files[0])
        } else {
            $('#img_load').attr('src', '')
        }
    })
}

function getOffset(el) {
    var _x = 0
    var _y = 0
    while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
        _x += el.offsetLeft - el.scrollLeft
        _y += el.offsetTop - el.scrollTop
        el = el.offsetParent
    }
    return {top: _y, left: _x}
}

window.info = {}
window.lastWindow = 0

function handler(e) {
    function cb(target, el, windowid) {
        if (info.target != target && !$(info.target).parents(`[data-floatwindowid="${windowid}"]`).length) {
            // console.log("REMOVE", windowid, info.currentWindow)
            info.visible = false
            el.animate({opacity: 0}, 100)
            setTimeout(() => el.remove(), 100)
            return
        }
        setTimeout(cb.bind({}, target, el, windowid), 1000)
    }

    info.action = ""

    if (e.target.tagName == "A" || $(e.target).parents("a").length) {
        if (e.target == info.target) {
            return
        }
        info.target = e.target

        if (e.target.dataset.userid)
            info.action = "profile"
        else
            return

        let data = {}
        switch (info.action) {
            case "profile":
                data = {id: e.target.dataset.userid}
                break
        }

        new Promise(function (resolve) {
            setTimeout(() => resolve(), parseInt(localStorage.getItem("float_window_wait")) || 1000)
        }).then(() => {
            if (info.target != e.target) {
                return
            }

            if ((info.currentWindow == $(e.target).data('floatwindow')) && info.visible) {
                let el = $(`[data-floatwindowid="${$(e.target).data('floatwindow')}"]`)
                el.css({left: (e.pageX > ($(window).width() / 2) ? e.pageX - el.width() + 10 : e.pageX - 10) + "px"})
                return
            }

            $.ajax(`/users/${data.id}`).then(resp => {

                lastWindow += 1
                info.currentWindow = lastWindow

                $(e.target).data('floatwindow', info.currentWindow)

                let user = resp.result.user
                console.log(user)

                let el = $(`
                <div class="float_info" data-floatwindowid="${info.currentWindow}">
                    <div class="float_user">
                        <div class="left">
                            <h2 class="fullname">${user.full_name}</h2>
                            <div class="description">${user.description || "<i>Пользователь ничего о себе не написал</i>"}</div>
                        </div>
                        <div class="right">
                            <img class="avatar" src="${user.avatar}" />
                        </div>
                    </div>
                </div>
            `)
                el.appendTo(document.body)
                el.css({
                    opacity: 0
                })
                let box = info.target.getBoundingClientRect()
                let offset = getOffset(info.target)
                window.el = info.target
                console.log(box)
                if (box.top > ($(window).height() / 2) && (box.top - el.height()) > 0) {
                    // console.log("BOTTOM")
                    el.css({
                        top: offset.top - el.height() - 32 + "px",
                        left: offset.left + "px",
                    })

                } else {
                    // console.log("TOP")
                    el.css({
                        top: offset.top + box.height + "px",
                        left: offset.left + "px",
                    })
                }
                el.animate({opacity: 1}, 300)
                el.find(`pre code`).each((k, el) => hljs.highlightBlock(el))

                setTimeout(cb.bind({}, e.target, el, info.currentWindow), 1000)

                info.visible = true
            })
        })
    }

    info.target = e.target

}
