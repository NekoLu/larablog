class UserMailer < ApplicationMailer
  default from: 'Блог Лары Покровской <noreply@noreply.larapokrovskaya.ru>'

  def activation_email
    @user = params[:user]
    @url  = "https://larapokrovskaya.ru/users/#{@user.id}/activate?code=#{@user.activation_hash}"
    mail(to: @user.mail, subject: 'Активация аккаунта')
  end

  def approve_email
    @user = params[:user]
    mail(to: @user.mail, subject: 'Ваша заявка на доступ к "психолог-психологу" была одобрена')
  end

  def disapprove_email
    @user = params[:user]
    mail(to: @user.mail, subject: 'Ваша заявка на доступ к "психолог-психологу" была отклонена')
  end

  def answer_email
    @user = params[:user]
    @comment = params[:comment]
    mail(to: @user.mail, subject: "#{@comment.user.full_name} ответил(а) на ваш комментарий")
  end

end
