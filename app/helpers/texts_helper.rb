module TextsHelper
  def text(name)
    Text.find_by(name: name).text.html_safe
  end
end
