class ReviewsController < ApplicationController
  before_action :require_admin

  def create
    @review = ProductReview.new(review_params)
    unless @review.save
      return api_render code: 400, errors: @review.errors
    end
    api_render code: 201, result: { review: @review }
  end

  def edit
    @review = ProductReview.find(params[:id]) || not_found
  end

  def update
    @review = ProductReview.find(params[:id]) || not_found
    unless @review.update(review_params)
      return api_render code: 400, errors: @review.errors
    end
    redirect_to "/products"
  end

  def destroy
    @review = ProductReview.find(params[:id]) || not_found
    @review.destroy
    redirect_to "/products"
  end

  private

  def review_params
    params.require(:product_review).permit([:text, :author, :product_id])
  end
end
