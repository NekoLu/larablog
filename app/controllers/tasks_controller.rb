class TasksController < ApplicationController
  before_action :require_admin

  def index
    @tasks = Task.all
  end

  def decline
    @task = Task.find(params[:id])
    @user = User.find(@task.payload["user_id"])
    @user.update(is_psychologist: false)
    @task.update(status: :declined)
    UserMailer.with(user: @user).disapprove_email.deliver_later

    redirect_to '/tasks'
  end

  def accept
    @task = Task.find(params[:id])
    @user = User.find(@task.payload["user_id"])
    @user.update(is_psychologist: true)
    @task.update(status: :complete)
    UserMailer.with(user: @user).approve_email.deliver_later
    return redirect_to '/tasks'
  end
end
