class ProductsController < ApplicationController
  def index
    @products = Product.all
  end

  def edit
    @product = Product.find(params[:id])
    not_found unless @product
  end

  def new
  end

  def create
    @product = Product.new(product_params)
    unless @product.save
      return api_render code: 400, errors: @product.errors
    end
    if product_params[:image]
      @product.image.attach(product_params[:image])
    end
    api_render code: 201, result: { product: @product }
  end

  def update
    @product = Product.find(params[:id])
    return api_render code: 404, errors: {product: ["not found"]} unless @product
    unless @product.update(product_params)
      return api_render code: 400, errors: @product.errors
    end
    if product_params[:image]
      @product.image.attach(product_params[:image])
    end
    redirect_to "/products"
  end

  def destroy
    @product = Product.find(params[:id])
    return redirect_to "/products" unless @product
    @product.destroy
    redirect_to "/products"
  end

  private

  def product_params
    params.require(:product).permit([:name, :description, :price, :image, :link])
  end
end
