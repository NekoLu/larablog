# frozen_string_literal: true
class ImagesController < ApplicationController
    before_action :require_admin
    # protect_from_forgery except: :create
  
    def upload
      blob = ActiveStorage::Blob.create_after_upload!(
        io: params[:file],
        filename: params[:file].original_filename,
        content_type: params[:file].content_type
      )
  
      render json: { url: url_for(blob) }
    end
  end