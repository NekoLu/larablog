class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :set_current_user
  before_action :set_random_products
  add_flash_types :warn

  def set_current_user
    @Text = Text
    return unless session[:user_id]
    @user_current = User.find(session[:user_id])
    unless @user_current.activated
      flash.now.alert = "Мы отправили вам на почту ссылку для активации аккаунта. Пожалуйста, перейдите по ней."
    end
  end

  def require_admin
    redirect_to "/" unless @user_current and @user_current.is_admin
  end

  def api_render(args = {})
    defaults = {
        status: true,
        code: 200,
        errors: {},
        result: {}
    }
    args = defaults.merge(args)
    render json: {
        status: args[:status] ? 'ok' : 'fail',
        code: args[:code],
        errors: args[:errors],
        result: args[:result]
    }, status: args[:code]
  end

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end

  def set_random_products
    @sidebar_products = Product.order("RANDOM()").first(3)
  end
end
