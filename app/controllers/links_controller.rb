class LinksController < ApplicationController
  def show
    @link = Link.find_by(link_hash: params[:hash])
    return not_found unless @link

    redirect_to @link.product.link
  end
end
