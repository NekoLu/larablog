class TextsController < ApplicationController
  before_action :require_admin

  def index
    @texts = Text.all
  end

  def create
    @text = Text.new(text_params)
    unless @text.save
      return api_render code: 400, errors: @text.errors
    end
    api_render code: 201, result: { text: @text }
  end

  def update
    @text = Text.find(params[:id])
    api_render code: 404 unless @text
    unless @text.update(text_params)
      return api_render code: 400, errors: @text.errors
    end
    api_render code: 200, result: { text: @text }
  end

  def delete
    @text = Text.find(params[:id])
    api_render code: 404 unless @text
    @text.destroy
    api_render
  end

  private

  def text_params
    params.require(:text).permit([:name, :text])
  end
end
