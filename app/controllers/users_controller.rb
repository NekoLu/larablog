class UsersController < ApplicationController
  def new
  end

  def show
    @user = User.find(params[:id]) || not_found
    api_render result: { user: @user }
  end

  def create
    @user = User.new(user_params)
    unless @user.save
      return api_render errors: @user.errors, status: false, code: 400
    end

    if params[:psychologist_request]
      unless params[:comment] and !params[:comment].empty?
        @user.destroy!
        return api_render errors: { comment: ["must exists"] }, status: false, code: 400
      end
      @task = Task.create(task_type: :psychologist_request, status: :active, payload: { user_id: @user.id, comment: params[:comment]})
    end

    UserMailer.with(user: @user).activation_email.deliver_later

    session[:user_id] = @user.id

    api_render result: { user: @user, task:  @task}, code: 201
  end

  def update
    @user = User.find(params[:id])
    return api_render code: 404 unless @user
    return api_render code: 403 unless (@user_current and (@user_current.id == @user.id || @user_current.is_admin))

    old_mail = @user.mail

    unless @user.check_password(params[:old_password])
      return api_render code: 400, errors: {password: ["original password is wrong"]}
    end

    unless @user.update(user_params)
      return api_render code: 400, errors: @user.errors
    end

    if @user.mail != old_mail
      @user.update(activated: false)
      UserMailer.with(user: @user).activation_email.deliver_later
    end

    if user_params[:avatar]
      @user.avatar.attach(user_params[:avatar])
    end

    api_render result: { user: @user }
  end

  def destroy
  end

  def activate
    not_found unless @user_current
    @user = User.find_by(id: params[:user_id]) || not_found
    not_found unless @user.id == @user_current.id
    not_found unless params[:code] == @user.activation_hash
    @user.update(activated: true)
    redirect_to "/", notice: "Аккаунт активирован!"
  end

  def resend
    not_found unless @user_current
    UserMailer.with(user: @user_current ).activation_email.deliver_later
    api_render
  end

  def edit
    @user = User.find_by(id: params[:id]) || not_found
    redirect_to "/" unless @user == @user_current
  end

  private

  def user_params
    p = params.require(:user).permit([:name, :surname, :mail, :password, :password_confirmation, :description, :avatar])
    if p[:password].empty?
      p = p.to_h.except!(:password)
    end
    p
  end

  def user_edit_params
    params.require(:user).permit([:description])
  end
end
