class CommentsController < ApplicationController
  def show
    @comment = Comment.find(params[:id])
    api_render code: 404, status: false unless @comment

    api_render result: { comment: @comment }
  end

  def create
    api_render code: 403, status: false unless @user_current
    @comment = Comment.new(comment_params)
    @comment.user = @user_current
    if @comment.post.category != "psychologist" || (!@user_current.is_psychologist and !@user_current.is_admin)
      return api_render code: 403, errors: {user: ["forbidden"]}
    end
    unless @comment.save
      return api_render errors: @comment.errors, status: false, code: 400
    end
    if @comment.parent && @comment.parent.user != @comment.user
      UserMailer.with(user: @comment.parent.user, comment: @comment).answer_email.deliver_later
    end
    api_render code: 201, result: { comment: @comment }
  end

  def update
    api_render code: 403, status: false unless @user_current
    @comment = Comment.find(params[:id])
    api_render code: 404, status: false unless @comment
    api_render code: 403, status: false unless (@user_current.id == @comment.user.id || @user_current.is_admin)
    unless @comment.update(comment_params)
      return api_render errors: @comment.errors, code: 400, status: false
    end
    api_render result: { comment: @comment }
  end

  def destroy
    api_render code: 403, status: false unless @user_current
    @comment = Comment.find(params[:id])
    api_render code: 403, status: false unless (@user_current.id == @comment.user.id || @user_current.is_admin)
    @comment.destroy
    api_render
  end

  private

  def comment_params
    params.require(:comment).permit([:text, :post_id, :parent_id])
  end
end
