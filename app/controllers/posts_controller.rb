class PostsController < ApplicationController

  def index
    limit = 10
    @posts = Post.blog.limit(limit).offset(params[:offset])
  end

  def psychologist_index
    limit = 10
    @posts = Post.psychologist.limit(limit).offset(params[:offset])
  end

  def book_index
    limit = 10
    @posts = Post.book.limit(limit).offset(params[:offset])
  end

  def new
    @post = Post.new
  end

  def show
    @post = Post.find(params[:id])
    if @post.category == "psychologist" and (!@user_current || (!@user_current.is_psychologist and !@user_current.is_admin))
      return redirect_to '/'
    end
    post_index = Post.where(category: @post.category).index(@post)
    @prev_post = Post.where(category: @post.category)[post_index - 1] if post_index > 0
    @next_post = Post.where(category: @post.category)[post_index + 1] if post_index < Post.all.count - 1

    @posts_like = Post.where(category: @post.category).any_tags(@post.tags) - [@post]
    @posts_like.sort { |a, b| compare(@post.tags, b.tags) <=> compare(@post.tags, a.tags)}
    @posts_like = @posts_like[0..2]
  end

  def create
    @post = Post.create post_params
    if params[:preview]
      puts 'Yay, preview'
      @post.preview.attach(params[:preview])
    end
    redirect_to @post
  end

  def edit
    @post = Post.find(params[:id])
  end

  def update
    @post = Post.find(params[:id])
    @post.update(post_params)
    redirect_to @post
  end

  def destroy
    puts params
    @post = Post.find(params[:id])
    @post.destroy
    redirect_to '/'
  end

  def search
    posts = Post.search_everywhere(params[:query])
    @posts = posts

    posts.each do |post|
      if post.category=='psychologist' && (!@user_current || (@user_current && !(@user_current.is_admin || @user_current.is_psychologist)))
        @posts -= [post]
      end
    end

    @posts
  end

  def tags_search
    posts = Post.all_tags([params[:tag]])
    @posts = posts

    posts.each do |post|
      if post.category=='psychologist' && (!@user_current || (@user_current && !(@user_current.is_admin || @user_current.is_psychologist)))
        @posts -= [post]
      end
    end

    @posts
  end

  def read
    @post = Post.find(params[:id])
    @post.update(views_count: @post.views_count+1)
    render json: {ok: true}
  end

  private

  def post_params
    r = params.require(:post).permit([:title, :short_text, :text, :tags, :preview, :category, :allow_comments])
    unless r[:tags].nil?
      unless r[:tags].index(',').nil?
        r[:tags] = r[:tags].gsub!(/, */, ",").split(",")
      else
        r[:tags] = [r[:tags]]
      end
    end
    puts r
    r
  end

  def compare(target, arr)
    x = 0
    arr.each do |item|
      x += 1 if target.include? item
    end
    x
  end
end
