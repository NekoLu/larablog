class SessionController < ApplicationController
  def login
    return unless set_user_by_mail_or_login

    unless @user.check_password(user_params[:password])
      return api_render errors: { password: ['is wrong'] }, status: false, code: 400
    end

    session[:user_id] = @user.id
    api_render result: { user: @user }
  end

  def logout
    session[:user_id] = nil
    redirect_to "/"
  end

  private

  def user_params
    params.require(:user).permit([:mail, :password])
  end

  def set_user_by_mail_or_login
    @user ||= User.find_by_mail(user_params[:mail])
    unless @user
      api_render errors: { user: ['not found'] }, status: false, code: 404
      return false
    end

    true
  end

end
